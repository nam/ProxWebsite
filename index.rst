ProxToolbox: fixed point iterations with proximal operators
###########################################################

This collection of Python/Matlab files used to generate many if not all of the
numerical experiments conducted in the papers in the Literature folder.  For a
complete listing of papers with links go to `publications
<http://vaopt.math.uni-goettingen.de/en/publications.php>`__.

This site is maintained by the `Working Group in Variational Analysis
<http://vaopt.math.uni-goettingen.de/en>`__ of the `Institute for Numerical and
Applied Mathematics <https://www.uni-goettingen.de/de/85746.html>`__ at the
`University of Göttingen <https://www.uni-goettingen.de/en/1.html>`__.

.. raw:: html

  <center>
    <video width="320" height="240" controls>
      <source src="https://num.math.uni-goettingen.de/proxtoolbox/media/JWST.mp4" type="video/mp4">
      <source src="https://num.math.uni-goettingen.de/proxtoolbox/media/JWST.ogg" type="video/ogg">
      <source src="https://num.math.uni-goettingen.de/proxtoolbox/media/JWST.webm" type="video/webm">
      Your browser does not support the video tag.
    </video>
    <p>Video: Representative iterate of a noisy JWST test wavefront recovered with the
    Douglas-Rachford algorithm.</p>
  </center>

Python version (still in development)
=====================================

Current (0.2)
-------------

`Source <https://num.math.uni-goettingen.de/proxtoolbox/tarballs/0.2/proxpython-0.2.tar.gz>`__

`Documentation <https://num.math.uni-goettingen.de/proxtoolbox/versions/0.2>`__

The documentation includes a tutorial.

Older versions
--------------

- 0.1 (`Source <https://num.math.uni-goettingen.de/proxtoolbox/tarballs/0.1/proxpython-0.1.tar.gz>`__,
  `Documentation <https://num.math.uni-goettingen.de/proxtoolbox/versions/0.1>`__)

Development version
-------------------

The source code is hosted on `GitLab
<https://gitlab.gwdg.de/nam/ProxPython>`__. Bugs can be reported on the `issue
tracker <https://gitlab.gwdg.de/nam/ProxPython/issues>`__.

Matlab version (3.0, August 2017)
=================================

`Source <http://vaopt.math.uni-goettingen.de/en/software/ProxMatlab-Release3.0.tar.gz>`__

For help with the Matlab version, see the :code:`README` file in the ProxMatlab
source.

Demos Accompanying Publications:
--------------------------------

- Cone and Sphere benchmark (`Source <http://vaopt.math.uni-goettingen.de/en/software/ProxMatlab-Cone_and_Sphere.tar.gz>`__):
  The demos are in the subfolder Cone_and_Sphere.  These are the codes generating the benchmarks in the article 
  `Optimization on Spheres: Models and Proximal Algorithms with Computational Performance Comparisons <https://epubs.siam.org/doi/abs/10.1137/18M1193025>`__ 
  by D.R. Luke, S. Sabach and M. Teboulle, SIAM J. Mathematics of Data Science, 1(3), 408-445 (2019).


- Nanoscale Photonic Imaging demos (`Source <http://vaopt.math.uni-goettingen.de/en/software/ProxMatlab-Cone_and_Sphere.tar.gz>`__):
  The demos are in the subfolder Nanosclae_Photonic_Imaging.  These are the codes accompanying the tutorial chapter by D. R. Luke 
  'Proximal Methods for Image Processing' pp. 165-202 (2020).
  In T. Salditt, A. Egner and D.R. Luke (eds) `Nanoscale Photonic Imaging <https://link.springer.com/book/10.1007/978-3-030-34413-9>`__.
  Topics in Applied Physics, vol 134. Springer, Cham.


Additional binary data
======================

- `Phase <http://vaopt.math.uni-goettingen.de/data/Phase.tar.gz>`_ (28mb)
- `Ptychography <http://vaopt.math.uni-goettingen.de/data/Ptychography.tar.gz>`_ (417mb)
- `CT <http://vaopt.math.uni-goettingen.de/data/CT.tar.gz>`_ (1.7mb)

The binary data need to be unpacked in the :code:`InputData` subdirectory
source code. For example, the :code:`Phase` data in ProxMatlab would be in
:code:`ProxMatlab/InputData/Phase`.

Acknowledgements
================

- Main Author:  Russell Luke, University of Göttingen
    
- Contributors:

	 Matthijs Jansen, I. Institute for Physics, University of Göttingen (Orbital Tomography);
	 Alexander Dornheim, Inst. for Numerical and Applied Math, Universität Göttingen (python);
         Stefan Ziehe, Inst. for Numerical and Applied Math, Universität Göttingen (python);
	 Rebecca Nahme, Inst. for Numerical and Applied Math, Universität Göttingen (python);
	 Matthew Tam, CARMA, University of Newcastle, Australia (Ptychography);
         Pär Mattson, Inst. for Numerical and Applied Math, Universität Göttingen (Ptychography);
	 Robin Wilke, Inst. for X-Ray Physics, Univesität Göttingen (Ptychography and Phase);
	 Robert Hesse, Inst. for Numerical and Applied Math, Universität Göttingen;
	 Alexander Stalljahn, Inst. for Numerical and Applied Math, Universität Göttingen (Sudoku).

- Funding:
	
	This has grown over the years and has been supported in part by:

		NASA grant NGT5-66;
		the Pacific Institute for Mathematical Sciences (PIMS);
		USA NSF Grant DMS-0712796;
		German DFG grant SFB-755 TPC2;
		German Israeli Foundation (GIF) grant G-1253-304.6/2014.





